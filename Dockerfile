FROM php:7.2-cli
RUN apt-get update -y && apt-get install -y libmcrypt-dev
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo mbstring
RUN apt-get install -y git
RUN apt-get install -y zip
WORKDIR /app
COPY . /app
RUN composer install --prefer-dist
EXPOSE 8800
CMD php bin/console server:run 0.0.0.0:8800
